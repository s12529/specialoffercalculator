
public class Basket {
	
	private final int MAX_BASKET_SIZE = 100;
	private Product[] products;
	private int productIndex;
	private double value = 0;	
	
	public Basket() {
		products = new Product[MAX_BASKET_SIZE];
		productIndex = 0;
	}
	
	public void addProduct(Product product) 
	{
		if(productIndex < MAX_BASKET_SIZE)
		{
			products[productIndex] = product;
			productIndex++;
		} else 
			System.out.println("Ju� nic nie zmie�ci si� do koszyka!");
	}
	
	
	public void printBusket() 
	{
		String boolDiscount;
	if(productIndex != 0) {
			for(int i=0; i<productIndex; i++) 
			{
				
				if(products[i].isDiscount() == true)
				{
					boolDiscount = "tak";
				} else boolDiscount = "nie";
				
			
				System.out.println(products[i].getName() + ", cena/szt: " + products[i].getPrice() + "z� , ilo��: " 
			+ products[i].getQuantity() + ", promocja: " + boolDiscount + ", razem: " + countProduct(i));
			}
		}
			else System.out.println("W koszyku nic nie ma!");
		
	}
		
	private double countProduct(int i) {
		return (products[i].getPrice() * (100 - products[i].getDiscountValue())/100) * products[i].getQuantity();
		} 
	
	public double countValue() {
		
		setValue(0);
		
		for(int i=0; i<productIndex; i++) {			
				setValue(getValue() + countProduct(i));
						
		}		
		return getValue();
	}
	
	public double promoCode(String key) {
		String promoCode = "JAVA";
		int discount = 5;
	
		if(key.equals(promoCode)) {
			
			System.out.println("Kod prawid�owy. Przyznano rabat " +  discount + "%.");
			setValue(((getValue() * (100 - discount))/100));
		}
		else System.out.println("Kod nieprawid�owy :(.");
		
		return getValue();
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}
	

}
