import java.util.Scanner;

public class Shop {

	public static final int EXIT = 0;
	public static final int ADD_PRODUCT = 1;
	public static final int SHOW_PRODUCTS = 2;
	public static final int ENTER_PROMO_CODE = 3;
	
	public static void main(String[] args) {
		
		Scanner Input = new Scanner(System.in);		
		Basket koszyk = new Basket();
		Product[] magazine = new Product[10];
		
		int option = -1;
		int productOption = 0;
		
		magazine[0] = new Product("Frugo", 9999, 3, true, 15);
		magazine[1] = new Product("Woda mineralna gazowana", 9999, 1.5, false, 0);
		magazine[2] = new Product("Baton Mars", 9999, 2.3, false, 0);
		magazine[3] = new Product("Bu�ki", 9999, 0.8, false, 0);
		magazine[4] = new Product("Pomara�cze", 9999, 1, true, 20);
		magazine[5] = new Product("Tymbark", 9999, 2, true, 10);
		magazine[6] = new Product("Milky Stars", 9999, 1.20, false, 0);
		magazine[7] = new Product("Pomidory", 9999, 0.70, false, 0);
		magazine[8] = new Product("Pizza mro�ona", 9999, 7.20, true, 10);
		magazine[9] = new Product("Banany", 9999, 1.20, false, 0);
		
		
		while (option != EXIT) {
			
			System.out.println("Witaj! W�a�nie trzymasz koszyk. Co chcesz zrobi�?");
			System.out.println("0 - Id� do kasy.");
			System.out.println("1 - W�� produkt do koszyka.");
			System.out.println("2 - Zobacz co masz w koszyku.");
			System.out.println("3 - Wprowad� kod promocyjny.");
			System.out.println("Warto�� twojego koszyka to " + koszyk.getValue() + " z�.");
			
			option = Input.nextInt();
			Input.nextLine();
			
			switch(option) {
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			case EXIT: 
			System.out.println("Zapraszam do kasy!");
			break;
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			case ADD_PRODUCT:
			
			Product product = new Product();
			
			System.out.println("Wybierz dost�pny produkt:");
			
			for(int i=0; i<magazine.length;i++)
			{
				System.out.println(i+1 + ". " + magazine[i].getName() + ", dost�pnych sztuk: " + magazine[i].getQuantity() + ", cena: " + magazine[i].getPrice()
						+ "z�, promocja: " + magazine[i].isDiscount() + ", Zni�ka: " + magazine[i].getDiscountValue() + "%.");
			}
			
			productOption = Input.nextInt();
			Input.nextLine();
		
			System.out.println("Ile sztuk chcesz kupi�?");
			
			int pieces = Input.nextInt();
			Input.nextLine();
			
			
				if (productOption>0 && productOption < magazine.length+1)
				{
				productOption--;
				product = new Product(magazine[productOption].getName(), pieces, magazine[productOption].getPrice(), magazine[productOption].isDiscount(), 
						magazine[productOption].getDiscountValue());
				
				int tmp = magazine[productOption].getQuantity() - pieces;
				magazine[productOption].setQuantity(tmp);
				
				} else System.out.println("Nieprawid�owa opcja.");

			
			
			/* <---- definicja produktu przez u�ytkownika
			System.out.print("Podaj nazw� produktu: ");
			product.setName(Input.nextLine());
			System.out.println("Podaj cen� produktu: ");
			product.setPrice(Input.nextDouble());
			Input.nextLine();
			System.out.println("Podaj ilo�� produkt�w: ");
			product.setQuantity(Input.nextInt());
			Input.nextLine();
			System.out.println("Czy jest w promocji? true/false");
			product.setDiscount(Input.nextBoolean());
			Input.nextLine();
			System.out.println("Podaj w procentach wartosc znizki: ");
			product.setDiscountValue(Input.nextDouble());
			Input.nextLine();
			*/
			koszyk.addProduct(product);
			koszyk.countValue();
	
			break;
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			case SHOW_PRODUCTS:
				koszyk.printBusket();
				break;
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			case ENTER_PROMO_CODE:
				
				System.out.println("Po dodaniu produktu, kody zni�kowe nale�y wprowadzi� jeszcze raz!");
				System.out.println("Wprowad� sw�j kod: ");
				koszyk.promoCode(Input.nextLine());
				
			break;
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			default:  System.out.println("Ups... co� chyba posz�o nie tak.");
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////				
			}
					
		}	
		Input.close();	
	}

}
